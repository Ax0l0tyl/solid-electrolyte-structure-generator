#Pymatgen/VASP Lithium iRemoval POSCARs with Li3YCl6 as starting point

import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from pymatgen.analysis.structure_matcher import StructureMatcher

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.transformations.standard_transformations import PartialRemoveSpecieTransformation

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYC = LYC * (2,1,1)

#This transformation automatically decorates a structure with oxidation states using a bond valence approach.
trans = AutoOxiStateDecorationTransformation()
LYC_oxi = trans.apply_transformation(LYC)

#Order a disordered structure. The disordered structure must be oxidation state decorated for ewald sum to be computed. 
#No attempt is made to perform symmetry determination to reduce the number of combinations.
#Can generate a list of best structures, currently selects the best calculated
trans = PartialRemoveSpecieTransformation("Li+",(1/3),algo=0)
LYC_Pred = trans.apply_transformation(LYC_oxi, return_ranked_list=5)

#Find Symmetrical structure duplicates
N=[]
for i in range(len(LYC_Pred)):
     for j in range(i+1,len(LYC_Pred)):
        if  StructureMatcher().fit(LYC_Pred[i]["structure"],LYC_Pred[j]["structure"]):
            print(i+1,"equals",j+1) 
            N.append(j)

#Remove duplicates from Structure List
N=list(dict.fromkeys(N))
for index in sorted(N, reverse=True):
    del LYC_Pred[index]

#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(len(LYC_Pred)):
    print(LYC_Pred[i]["energy"])
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(LYC_Pred[i]["structure"].to(fmt="CIF"))


