from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from pymatgen.analysis.ewald import EwaldSummation
from pymatgen.analysis.structure_matcher import StructureMatcher

#import Li3YCl6 from cif fileimport easygui
Num = 5
Direc = easygui.diropenbox()
Structures = [0]*Num

for i in range(Num):
    j = i + 1
    Struc = str(Direc + "\\" + str(j) + "\POSCAR")
    LYC = Structure.from_file(Struc)
    Structures[i] = LYC
    LYC.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1}) 
    Esum = EwaldSummation(LYC)
    print(Esum.total_energy)

for i in range(Num):
    for j in range(i+1,Num):
        if  StructureMatcher().fit(Structures[i],Structures[j]):
            print(i+1,"equals",j+1)


