import numpy
import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.ewald import EwaldSummation

def add_li(LYC):
    #check how many atoms need to be removed for each site_name
    import random
    import copy
    	    
    while True:
        try:
            c = copy.deepcopy(LYC)
            for j in range(12):
                c.insert(0,'Li',[round(random.random(), 5),round(random.random(), 5),round(random.random(), 5)],validate_proximity=True)
        except ValueError: 
            print('rerun')
        else:
            break
    return c

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYCFull = Structure.from_file(easygui.fileopenbox())
z=0
Li =  LYC[0].species
b = []
Ts = 5
PA= 5

Ewald_low = [0]*Ts #Lowest Esum
Strucs = [LYC]*Ts
Final_Strucs = [LYC]*Ts

#print(Strucs)

for i in range(len(LYCFull)):
    if LYCFull[i].species == Li:
        Test = True
        for j in range(len(LYC)):
            if LYC[j] == LYCFull[i]:
               Test = False
               continue
        if Test == True:  
            LYC.insert(0,'Li',LYCFull[i].coords,coords_are_cartesian=True)
            
for j in range(10000):
    z = z + 1
    print(z)
    LYCAdd = add_li(LYC)
    LYCAdd.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1}) 
    ESum = EwaldSummation(LYCAdd)
    for val_low in Ewald_low:
        if ESum.total_energy < val_low:
            replace_low = Ewald_low.index(max(Ewald_low))
            Ewald_low[replace_low] = ESum.total_energy
            Strucs[replace_low] = LYCAdd
            #print('new')
            break

 
sort_vec = numpy.argsort(Ewald_low)
Ewald_low.sort()
print(Ewald_low)  

for i in range(Ts):
    a = sort_vec[i]
    Final_Strucs[i] = Strucs[a]
       
#print(Final_Strucs)          
#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(PA):
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(Final_Strucs[i].to(fmt="POSCAR"))


