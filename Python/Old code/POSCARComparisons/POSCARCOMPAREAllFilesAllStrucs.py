import os
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from pymatgen.analysis.ewald import EwaldSummation
from pymatgen.analysis.structure_matcher import StructureMatcher

#import Li3YCl6 from cif fileimport easygui
Num = 5
DirecListLi = []
Direc = easygui.diropenbox()
DirecList = os.listdir(Direc)

Structures = []
Name = []

for i in DirecList:
    if "Li" in i:
        DirecListLi.append(i)

print(DirecListLi)

for x in DirecListLi:
    bad = False
    for i in range(Num):
        j = i + 1
        Struc = str(Direc + "\\" + x + "\\" + str(j) + "\CONTCAR")
        StrucName = str( x + "\\" + str(j))
        try:
            LYC = Structure.from_file(Struc)
        except FileNotFoundError: 
            print('Failed:')
            print(x)
            bad = True
            break
        except ValueError: 
            print('EmptyPOSCAR:')
            print(x,j)
            continue
        Structures.append(LYC)
        Name.append(StrucName)
        #LYC.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1}) 
        #Esum = EwaldSummation(LYC)
        #print(Esum.total_energy)
    if bad:
        continue


for i in range(len(Name)):
    for j in range(i+1,len(Name)):
        if  StructureMatcher().fit(Structures[i],Structures[j]):
            print(Name[i],"equals",Name[j])

