#Pymatgen/VASP Lithium Addition POSCARs with Li4YCl6 as starting point

import numpy
import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.ewald import EwaldSummation

def add_li(LYC):
    #Add randomly generated Lithium sites to the structure
    import random
    import copy
    #Create loop to add # of Li sites randomly with a fail safe if sites are added to close to existing sites	    
    while True:
        try:
            #create a copy of LYC to keep LYC contact
            c = copy.deepcopy(LYC)
            #Generate and add site to the structure
            for j in range(12):
                c.insert(0,'Li',[round(random.random(), 5),round(random.random(), 5),round(random.random(), 5)])
        except ValueError: 
            print('rerun')
        else:
            break
    return c

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) 

z=0 #Counter
Ts = 5 #Total amount of structures to generate
PA= 5 #Amount to write to file

Ewald_low = [0]*Ts #Lowest Esum list
Strucs = [LYC]*Ts
Final_Strucs = [LYC]*Ts

#Generate new structures with sites added randomly            
for j in range(10000):
    z = z + 1
    print(z)
    #Create new strucutre using the Li4YCl6 structure
    LYCAdd = add_li(LYC)
    LYCAdd.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1}) 
    ESum = EwaldSummation(LYCAdd)
     #Determine Ewald energy and if this is lower than one of the energies in the list add and remove the higher energy structure
    for val_low in Ewald_low:
        if ESum.total_energy < val_low:
            replace_low = Ewald_low.index(max(Ewald_low))
            Ewald_low[replace_low] = ESum.total_energy
            Strucs[replace_low] = LYCAdd
            break

#Sort the structures in order of lowest to highest structures
sort_vec = numpy.argsort(Ewald_low)
Ewald_low.sort()
for i in range(Ts):
    a = sort_vec[i]
    Final_Strucs[i] = Strucs[a]
          
#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(PA):
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(Final_Strucs[i].to(fmt="POSCAR"))


