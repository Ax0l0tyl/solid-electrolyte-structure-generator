# -*- coding: UTF-8 -*-
import os
import re
from cavd.netio import *
from monty.io import zopen
from cavd.channel import Channel
from cavd.netstorage import AtomNetwork, connection_values_list
from cavd.local_environment import CifParser_new, LocalEnvirCom
import easygui


def remove_li(LYC,b):
    #check how many atoms need to be removed for each site_name
    import random
    import copy

    c = copy.deepcopy(LYC)
    keep = random.sample(b,1)
    
    for i in keep:
        c.remove(i)
   
    return c




def outVesta(migrant, ntol=0.02, rad_flag=True, lower=0.0, upper=10.0, rad_dict=None):
    filename = easygui.fileopenbox()
    with zopen(filename, "rt") as f:
        input_string = f.read()
    parser = CifParser_new.from_string(input_string)
    #stru = Structure.from_file(filename) # place directory if no easygui 
    stru = parser.get_structures(primitive=False)[0]
    stru.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})
    
    Li =  stru[len(stru)-1].species
    LiList= []
    for i in stru:
        if i.species == Li:
            LiList.append(i)
    print(stru)
    stru = remove_li(stru,LiList)
    print(stru)        
    #stru.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})

    species = [str(sp).replace("Specie ","") for sp in stru.species]
    elements = [re.sub('[^a-zA-Z]','',sp) for sp in species]
    #sitesym = parser.get_sym_opt()
    if migrant not in elements:
        raise ValueError("The input migrant ion not in the input structure! Please check it.")
    effec_radii,migrant_radius,migrant_alpha,nei_dises,coordination_list = LocalEnvirCom(stru,migrant)
    
    radii = {}
    if rad_flag:
        if rad_dict:
            radii = rad_dict
        else:
            radii = effec_radii
    
    atmnet = AtomNetwork.read_from_RemoveMigrantCif(filename, migrant, radii, rad_flag)
    vornet,edge_centers,fcs,faces = atmnet.perform_voronoi_decomposition(True, ntol)


    prefixname = filename.replace(".cif","")

    # compute the R_T
    conn_val = connection_values_list(prefixname+".resex", vornet)
    #channels = Channel.findChannels2(vornet, atmnet, lower, upper, prefixname+".net")
    
    # output vesta file for visiualization
    #Channel.writeToVESTA(channels, atmnet, prefixname)
    
    return conn_val

if __name__ == "__main__":
    # Calculate transport channel using given radii.
    #rad_dict = {"Li1": 0.73, "C1": 0.06, "O1": 1.24, "O2": 1.23}
    #conn_val = outVesta("examples/cifs/Li/icsd_16713.cif","Li",ntol=0.02, rad_flag=True, lower=0.0, upper=10.0, rad_dict= rad_dict)
   
    # Calculate transport channel using auto-calculate radii.
    conn_val =  outVesta("Li",ntol=0.02, rad_flag=True, lower=0.0, upper=10.0)
    print("conn_val:", conn_val)
    

