import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYC = LYC * (2,1,1)
#Remove Oxidation states
#LYC_Pred = LYC.remove_oxidation_states()
#print(LYC_Pred)
#generates POSCAR file and writes to it
fname = easygui.filesavebox(default=".cif")
f = open(fname, "w")
f.write(LYC.to(fmt="cif"))


