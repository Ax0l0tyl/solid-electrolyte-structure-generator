import numpy
import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox
from pymatgen.analysis.structure_matcher import StructureMatcher

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.ewald import EwaldSummation

def add_li(LYC,b):
    #check how many atoms need to be removed for each site_name
    import random
    import copy

    c = copy.deepcopy(LYC)
    keep = random.sample(b,3)
    
    for i in keep:
        c.insert(0,'Li',i,coords_are_cartesian=True)
        c.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})
    return c


#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYCFull = Structure.from_file(easygui.fileopenbox())
z=0
Li =  LYC[0].species
b = []
Ts = 50
PA= 50


Ewald_low = [-1000]*Ts #Lowest Esum
Strucs = [LYC]*Ts
Final_Strucs = [LYC]*Ts

#print(Strucs)

for i in range(len(LYCFull)):
    if LYCFull[i].species == Li:
        Test = True
        for j in range(len(LYC)):
            if LYC[j] == LYCFull[i]:
                Test = False
                continue
        if Test == True:  
            b.append(LYCFull[i].coords) 

            
for j in range(10000):
    z = z + 1
    print(z)
    LYCAdd = add_li(LYC,b)
    for k in range(Ts):
        if Strucs[k] == LYCAdd:
            #print('copy')
            break 
    #print(LYCAdd)
    else:
        ESum = EwaldSummation(LYCAdd)
        for val_low in Ewald_low:
            if ESum.total_energy > val_low:
                replace_low = Ewald_low.index(min(Ewald_low))
                Ewald_low[replace_low] = ESum.total_energy
                Strucs[replace_low] = LYCAdd
                #print('new')
                break

sort_vec = numpy.argsort(Ewald_low)
Ewald_low.sort()
print(Ewald_low)  

for i in range(Ts):
    a = sort_vec[i]
    Final_Strucs[i] = Strucs[a]

#Find Symmetrical structure duplicates
N=[]
for i in range(len(Final_Strucs)):
     for j in range(i+1,len(Final_Strucs)):
        if  StructureMatcher().fit(Final_Strucs[i],Final_Strucs[j]):
            print(i+1,"equals",j+1) 
            N.append(j)

#Remove duplicates from Structure List
N=list(dict.fromkeys(N))
for index in sorted(N, reverse=True):
    del Final_Strucs[index]
    del Ewald_low[index]

print(Ewald_low)
print(len(Ewald_low))
print(len(Final_Strucs))        
#print(Final_Strucs)          
#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(len(Ewald_low)):
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(Final_Strucs[i].to(fmt="POSCAR"))


