#Analyses the Ewald energy of multiple structures from a file directory

import os
import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))

#importing transformations and Ewald energy calculator required
from pymatgen.analysis.ewald import EwaldSummation

#import Li3YCl6 from cif fileimport easygui
Num = 5 #Number of structures in a directory
DirecListLi = []
Direc = easygui.diropenbox()
DirecList = os.listdir(Direc)

#Create a directory list of folders containing Li
for i in DirecList:
    if "Li" in i:
        DirecListLi.append(i)

print(DirecListLi)

#For each directory, calculate the Ewald energy per structure and print this value
for x in DirecListLi:
    print(x)
    bad = False
    for i in range(Num):
        j = i + 1
        Struc = str(Direc + "\\" + x + "\\" + str(j) + "\POSCAR")
        try:
            #import Li3YCl6 from cif fileimport easygui
            LYC = Structure.from_file(Struc) # place directory if no easygui 

            #Add oxidation states
            LYC.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})

            #This computes the ewald sum Energy of a structure
            ESum = EwaldSummation(LYC)
            print(ESum.total_energy)
        #Check for errors in case file is missing or no energy value available    
        except FileNotFoundError: 
            print('Failed:')
            print(x)
            bad = True
            break
        except ValueError: 
            print('EmptyPOSCAR:')
            print(x,j)
            continue
        if bad:
            continue
    







