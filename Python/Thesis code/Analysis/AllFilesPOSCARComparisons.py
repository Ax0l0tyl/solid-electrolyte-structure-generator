#Compares if structures are symmetrically equivalent reading from a file directory

import os
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from pymatgen.analysis.ewald import EwaldSummation
from pymatgen.analysis.structure_matcher import StructureMatcher

#import Li3YCl6 from cif fileimport easygui
Num = 5
DirecListLi = []
Direc = easygui.diropenbox()
DirecList = os.listdir(Direc)
Structures = []
Name = []

#Create a directery list of folders containing Li
for i in DirecList:
    if "Li" in i:
        DirecListLi.append(i)

print(DirecListLi)

#Import all structures to a structures Array
for x in DirecListLi:
    bad = False
    for i in range(Num):
        j = i + 1
        Struc = str(Direc + "\\" + x + "\\" + str(j) + "\CONTCAR")
        StrucName = str( x + "\\" + str(j))
        #import structure from POSCAR or CONTCAR
        try:
            LYC = Structure.from_file(Struc)
        #Check for errors in case file is missing or structure is available
        except FileNotFoundError: 
            print('Failed:')
            print(x)
            bad = True
            break
        except ValueError: 
            print('EmptyPOSCAR:')
            print(x,j)
            continue
        Structures.append(LYC)
        Name.append(StrucName)
    if bad:
        continue

#Compare if POSCARs are symetrically equivalent, if so these are printed
for i in range(len(Name)):
    for j in range(i+1,len(Name)):
        if  StructureMatcher().fit(Structures[i],Structures[j]):
            print(Name[i],"equals",Name[j])

