import pymatgen
import pandas as pd
from pymatgen import IStructure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox
import csv

#import Li3YCl6 from cif fileimport easygui
LYC = IStructure.from_file(easygui.fileopenbox()) # place directory if no easygui 



ind = 0
Ysites = []
Sites= LYC.sites
b = []
c= []
Yttrium = []
YttriumCl = []
Neighbours = []
ClNeighbours = []
ClNeighboursIndex = []

#Make list of all Yttrium sites
for i in LYC:
    if "Y" in i.species:
        Ysites.append(i)
    
#Find neighbours using pymatgen function
a = LYC.get_neighbor_list(3.5,Ysites,1, True)

#Find specific sites from neighbours tulp and get coordinates and atom sort first for yttrium then the nieghbours
for i in a[0]:
    Yttrium.append(Ysites[i])

for i in a[1]:
    Neighbours.append(Sites[i])

#Creating new list matrix from selected rows instead of all the data
for i in Neighbours:
    if "Cl" in i:
        ClNeighboursIndex.append(ind)
        ClNeighbours.append(i)
        YttriumCl.append(Yttrium[ind])
        b.append(a[3][ind])
    ind += 1

c.append(YttriumCl)
c.append(ClNeighbours)
c.append(b)

#Make final matrix only containing Cl as neigbours

c = zip(*c)


with open('my.csv','w', newline='') as out:
   csv_out=csv.writer(out)
   csv_out.writerows(c)