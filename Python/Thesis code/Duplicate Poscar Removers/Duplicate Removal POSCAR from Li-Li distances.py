#Removes Symmetrically identical POSCARs from a list of total Li-Li distances strucutures

import pymatgen
from pymatgen import Structure
import easygui 
from pymatgen.analysis.structure_matcher import StructureMatcher

Num=200
Direc = easygui.diropenbox()
Structures = [0]*Num

#Import structures
for i in range(Num):
    j = i + 1
    Struc = str(Direc + "\POSCAR_" + str(j))
    LYC = Structure.from_file(Struc)
    Structures[i] = LYC

#Reverse list since the total Li-Li list is sorted by lowest to highest
Structures.reverse()    

#Find Symmetrical structure duplicates
N=[]
for i in range(len(Structures)):
     for j in range(i+1,len(Structures)):
        if  StructureMatcher().fit(Structures[i],Structures[j]):
            print(i+1,"equals",j+1) 
            N.append(j)

#Remove duplicates from Structure List
N=list(dict.fromkeys(N))
for index in sorted(N, reverse=True):
    del Structures[index]

#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(len(Structures)):
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(Structures[i].to(fmt="POSCAR"))


