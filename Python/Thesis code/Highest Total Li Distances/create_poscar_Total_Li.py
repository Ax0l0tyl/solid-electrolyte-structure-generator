#%%
def frac2car(lattice, frac):
    import numpy    
# perhaps the [0][i], [1][i] and [2][i] must be interchanged in lattice[][]
    cart = numpy.dot(frac,lattice)
    
    return cart
#%%
def calc_dist(lattice, frac1, frac2):
# Calculate distance (in Angstrom) between two sets of fractional coordinates 
# including PBC!
    import numpy
    cart1 = frac2car(lattice, frac1)
    cart2 = frac2car(lattice, frac2)

    temp = [None]*3
    dist = 99999.999
    for i in [-1,0,1]: #PBC in a-direction
        for j in [-1,0,1]: #PBC in b
            for k in [-1,0,1]: #PBC in c
                temp = cart2 + numpy.dot([i, j, k],lattice)  
                dist_temp = numpy.sqrt((cart1[0]-temp[0])**2 +(cart1[1]-temp[1])**2 +(cart1[2]-temp[2])**2)
                if dist_temp < dist:
                    dist = dist_temp

    return dist
#%%
def read_in_poscar(poscar_file):
    #open file
    poscar = open(poscar_file, 'r')
    #Declare some stuff
    atoms = []
    coor = []#-100., -100., -100.]
    lattice = [[0.,0.,0.], [0.,0.,0.], [0.,0.,0.]]
    a = [0., 0., 0.]
    #[[None]*3]*3 #

    #loop over all lines  
    line_count = 0
    for line in poscar: 
        line_count += 1
        temp = line.split()
        if line_count > 2 and line_count < 6 : #The lattice
            lattice[line_count-3][0] =  float(temp[0])
            lattice[line_count-3][1] =  float(temp[1])
            lattice[line_count-3][2] =  float(temp[2])
            #print(float(line)
        elif line_count > 7: # the atom
            for i in range(0,3):
                a[i] = float(temp[i])
            # add:
            coor.append(a[:])
            atoms.append(temp[3])
            #print(atom_count)

    #print(lattice)        
    #print(atoms)
    #print(coor)        
    poscar.close() #close cif-file
    return lattice, coor, atoms

#%%       
def add_li(uc_frac, uc_atoms, nr_li, total_li_positions, total_atomic_positions, add_atoms):
    #check how many atoms need to be removed for each site_name
    import random
    part_frac = []
    part_atoms = []
    newLi = []
    check = True

    while check: 
        check = False
        for j in range(add_atoms):
            newLi.append([round(random.random(), 4),round(random.random(), 4),round(random.random(), 4)])
            for i in range(total_atomic_positions): #range(total_li): # the li atoms
            #if frac pos already exists:
                if uc_frac[i][:] == newLi[j][:]:
                     newLi = []
                     check = True   

    for i in range(add_atoms): #range(total_li): # the li atoms
        #if i in keep:
        part_frac.append(newLi[i][:])
        part_atoms.append('Li')  

         #the rest of the atoms
    for i in range(total_atomic_positions):       
        part_frac.append(uc_frac[i][:])
        part_atoms.append(uc_atoms[i])   

 #print(part_atoms)
 #print(part_sites)

    return part_frac, part_atoms  




#%%       
def remove_li(uc_frac, uc_atoms, nr_li, total_li_positions, total_atomic_positions):
    #check how many atoms need to be removed for each site_name
    import random
    part_frac = []
    part_atoms = []

    keep = random.sample(range(total_li_positions), nr_li)
    for i in keep: #range(total_li): # the li atoms
        #if i in keep:
        part_frac.append(uc_frac[i][:])
        part_atoms.append(uc_atoms[i])   
    for i in range(total_li_positions, total_atomic_positions): #the rest of the atoms       
        part_frac.append(uc_frac[i][:])
        part_atoms.append(uc_atoms[i])   

 #   print(part_atoms)
 #  print part_sites

    return part_frac, part_atoms   
#%%
def calc_total_dist(frac, lattice, nr_li):
    #Calculates the electrostatic energy using the shifted potential method
    frac1 = [None]*3
    frac2 = [None]*3

    dist_tot = 0.0
    for i in range(nr_li): #Change to loop only over Li-atoms
        frac1 = frac[i]
        for j in range(i,nr_li): #start at i to avoid double counting
            frac2 = frac[j]
            dist_tot += calc_dist(lattice, frac1, frac2)            
            
    return dist_tot

#%%
def rotate(l, n):
    return l[n:] + l[:n]
#%%
def write_poscar(outfile, lattice, uc_sites, uc_atoms): 
    
    species_names = [None]*20
    nr_per_species = [0]*20 #? len(???) possible? 
    species_count = 0
    for i in range(len(uc_atoms)): #len(uc_atoms):    
        found = False
        for j in range(len(species_names)): #len(species_names)
            if uc_atoms[i] == species_names[j]: #Same species as found earlier:
                nr_per_species[j] += 1
                found = True
        if not found: #new species name
            species_names[species_count] = uc_atoms[i]
            nr_per_species[species_count] += 1
            species_count += 1
    #print 'Species names: ', species_names, species_count
    #print 'Nr. per species: ', nr_per_species
    
    poscar = open(outfile, 'w')
    poscar.write(outfile + " fractional occupancies " + str(species_names[0:species_count]) + " \n" ) #title
    poscar.write("  1.00 \n") #scaling factor
    for i in range(3):
        poscar.write( "  " + str(lattice[i][0]) + '  ' + str(lattice[i][1]) + '   ' + str(lattice[i][2]) + " \n") #?or [:][i]?
    for i in range(species_count):
        poscar.write(str(nr_per_species[i]) + "  ")
    poscar.write(" \n")
    poscar.write("  Direct \n")
    for i in range(species_count):    
        for j in range(len(uc_atoms)):
            if uc_atoms[j] == species_names[i]:
                poscar.write("  " + str(uc_sites[j][0]) + '   ' + str(uc_sites[j][1]) + '   ' + str(uc_sites[j][2]) + '   ' + uc_atoms[j] + " \n") #Fractional coor + species name    
    
    poscar.close()    
    return

#%%   

def Make_Poscar_Files(path, nr_tries, nr_structures_high, nr_structures_low, nr_structures_mid, AR, add_atoms, remove_element, poscar_file):
    
    import numpy
    import time
    import os
    from collections import OrderedDict

    # Read in POSCAR:
    lattice, uc_frac, uc_atoms = read_in_poscar(poscar_file)
    # Which atoms do we have in what order
    atoms_in_material = list(OrderedDict.fromkeys(uc_atoms))
    # Where is the element that we need
    remove_element_index = atoms_in_material.index(remove_element)
    # How many atoms are there of each element
    amount_element = []
    for element in atoms_in_material:
        amount_element.append(uc_atoms.count(element))
        
    if remove_element != 0:
        loop_over = range(remove_element_index)
        counter = 0
        for i in loop_over:
            counter += amount_element[i]
        uc_frac = rotate(uc_frac, counter)
        uc_atoms = rotate(uc_atoms, counter)
        atoms_in_material = rotate(atoms_in_material, remove_element_index)
        amount_element = rotate(amount_element, remove_element_index)  
        remove_element_index = 0

    #If atoms need to be removed add_atoms need to be negative
    if AR == False:
        add_atoms = -add_atoms
    # total amount of atoms in POSCAR    
    total_atomic_positions = len(uc_atoms)
    # How many atoms after adding
    total_atoms = total_atomic_positions + add_atoms
    # How many atoms of the element after adding as many as specified
    total_li_positions = amount_element[remove_element_index]
    nr_li = total_li_positions + add_atoms
    
    print('Generating ', nr_tries, ' random structures.') 
    print('Keeping the ', nr_structures_high, ' with the highest Li-Li distance.')
    print('Keeping the ', nr_structures_low, ' with the lowest Li-Li distance.')
    print('Keeping the ', nr_structures_mid, ' with the middle Li-Li distance.')
    print('Nr. of Li-atoms in the structures: ', nr_li)
    
    distances_high = [0.0]*nr_structures_high #High Li-Li distance
    frac_high = [[None]*total_atoms]*nr_structures_high
    atoms_high = [[None]*total_atoms]*nr_structures_high
    distances_low = [999999]*nr_structures_low #Low Li-Li distance
    frac_low = [[None]*total_atoms]*nr_structures_low
    atoms_low = [[None]*total_atoms]*nr_structures_low
    distances_mid = [0.0]*nr_structures_mid #mid Li-Li distance
    frac_mid = [[None]*total_atoms]*nr_structures_mid
    atoms_mid = [[None]*total_atoms]*nr_structures_mid
    start = time.time()
    
    
    
    for i in range(nr_tries): 
        # 3) remove Li-atoms randomly:
        if AR == True:
            part_frac, part_atoms = add_li(uc_frac, uc_atoms, nr_li,  total_li_positions, total_atomic_positions, add_atoms)    
        if AR == False:   
            part_frac, part_atoms = remove_li(uc_frac, uc_atoms, nr_li,  total_li_positions, total_atomic_positions)
        # 4) calculate energy of the generated structure:
        try_dist = calc_total_dist(part_frac, lattice, total_atoms)
           
        # 5)If try_dist in highest nr_structures until now --> remember the stuff needed for the .cif and POSCAR files
        # For high Li-Li distnance
        for val_high in distances_high:  
            if try_dist > val_high: #Higher than one of the previously found structures
                replace_high = distances_high.index(min(distances_high))
                distances_high[replace_high] = try_dist
                frac_high[replace_high] = part_frac            
                atoms_high[replace_high] = part_atoms
                print('Higher total Li-Li distance found at step ', i, ', distance: ', try_dist)
                break
            
        # For Low Li-Li distnance       
        for val_low in distances_low:   
            if try_dist < val_low:
                replace_low = distances_low.index(max(distances_low))
                distances_low[replace_low] = try_dist
                frac_low[replace_low] = part_frac            
                atoms_low[replace_low] = part_atoms
                print('Lower total Li-Li distance found at step ', i, ', distance: ', try_dist)
                break #exit for loop
                
        # For middle Li-Li distnance    
                
        mid = (max(distances_high)+min(distances_low))/2  #determine the middle in every step
        
        for val_mid in distances_mid:
            if abs(try_dist-mid) < abs(val_mid-mid):
                mid_ind = abs(distances_mid-mid)
                replace_mid = list(mid_ind).index(max(mid_ind))
                distances_mid[replace_mid] = try_dist
                frac_mid[replace_mid] = part_frac            
                atoms_mid[replace_mid] = part_atoms
                print('Mid total Li-Li distance found at step ', i, ', distance: ', try_dist)
                break                
    
        if (numpy.mod(i, nr_tries/100)) == 0: #write a * after 1 percent of the simulation is done
            print('*')#, 
     
    end = time.time()       
    print('Average time per calculation: ', (end-start)/nr_tries)   
     
    # 6) Write outputfiles for the nr_structures with lowest energy sorted by energy (nr. 1 is lowest, 2 second lowest, etc.) 
    
    final_frac_high = [[None]*len(part_atoms)]*nr_structures_high
    final_atoms_high = [[None]*len(part_atoms)]*nr_structures_high
    final_frac_low = [[None]*len(part_atoms)]*nr_structures_low
    final_atoms_low = [[None]*len(part_atoms)]*nr_structures_low
    final_frac_mid = [[None]*len(part_atoms)]*nr_structures_mid
    final_atoms_mid = [[None]*len(part_atoms)]*nr_structures_mid
    
    #Sort the low_structures by energy, nr. 1 has the lowest energy
    
    sort_vec_high = numpy.argsort(distances_high)
    distances_high.sort()
    sort_vec_low = numpy.argsort(distances_low)
    distances_low.sort()
    sort_vec_mid = numpy.argsort(distances_mid)
    distances_mid.sort()
    for i in range(nr_structures_high):
        final_frac_high[sort_vec_high[i]] = frac_high[i]
        final_atoms_high[sort_vec_high[i]] = atoms_high[i]
    for i in range(nr_structures_low):
        final_frac_low[sort_vec_low[i]] = frac_low[i]                   ############## This is where it goes wrong
        final_atoms_low[sort_vec_low[i]] = atoms_low[i]                 
    for i in range(nr_structures_mid):
        final_frac_mid[sort_vec_mid[i]] = frac_mid[i]
        final_atoms_mid[sort_vec_mid[i]] = atoms_mid[i]
    
    print('Total distances of the highest structures: \n', distances_high)
    print('Total distances of the lowest structures: \n', distances_low)
    print('Total distances of the mid structures: \n', distances_mid)
    
    #Write output
    
    #Define the name of the directory to be created
    path_high = path + r"/High" 
    path_low = path + r"/Low" 
    path_mid = path + r"/Mid" 
    
    try:  
        os.mkdir(path)
        os.mkdir(path_high)
        os.mkdir(path_low)
        os.mkdir(path_mid)
    except OSError:  
        print ("Creation of the directory %s failed" % path)
    else:  
        print ("Successfully created the directory %s " % path)
        
    #Create POSCARs for High mid and low distances    
        
    for i in range(nr_structures_high):
        #For use in VASP calculations:
        outfile_poscar = path + r'/High/POSCAR_%s'% (i+1)
        write_poscar(outfile_poscar, lattice, final_frac_high[i], final_atoms_high[i])
        
    for i in range(nr_structures_low):
        outfile_poscar = path + r'/Low/POSCAR_%s' % (i+1)
        write_poscar(outfile_poscar, lattice, final_frac_low[i], final_atoms_low[i])
        
    for i in range(nr_structures_mid):
        outfile_poscar = path + r'/Mid/POSCAR_%s' % (i+1)
        write_poscar(outfile_poscar, lattice, final_frac_mid[i], final_atoms_mid[i])
        
    print('nr. ', nr_structures_high, 'has the highest Li-Li distances, followed by nr. ', nr_structures_high-1, ' etc.,')
    print('Nr. of Li-atoms in the structures: ', nr_li)
    print('Done, exiting')   

#%%
    
