import os
import pymatgen
from pymatgen.io.vasp.outputs import Oszicar
import easygui #To open files (can also be done manually if not installed (pip install easygui))

#import Li3YCl6 from cif fileimport easygui
Num = 5
DirecListLi = []
Direc = easygui.diropenbox()
DirecList = os.listdir(Direc)

for i in DirecList:
    if "Li" in i:
        DirecListLi.append(i)

print(DirecListLi)

for x in DirecListLi:
    print(x)
    bad = False
    for i in range(Num):
        j = i + 1
        Struc = str(Direc + "\\" + x + "\\" + str(j) + "\OSZICAR")
        try:
            Os = Oszicar(Struc)
            E = Os.final_energy
            print(float(E))
        except FileNotFoundError: 
            print('Failed:')
            print(x)
            bad = True
            break
        except IndexError: 
            print('Index Error:')
            print(x,j)
            continue
        if bad:
            continue
    


