from pymatgen.io.vasp.outputs import Oszicar
import easygui #To open files (can also be done manually if not installed (pip install easygui))

#Specify amount of files wtih Oszicars to read and the direc containing the folder
Num = 5
Direc = easygui.diropenbox()

#Display Free Energy as float
for i in range(Num):
    j = i + 1
    Struc = str(Direc + "\\" + str(j) + "\OSZICAR")
    Os = Oszicar(Struc)
    E = Os.final_energy
    print(float(E))
