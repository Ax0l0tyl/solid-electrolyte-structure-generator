#Standard library imports
import numpy
import pymatgen
import hiphive
import time
import random
import copy

#importing transformations/calculations required
from pymatgen import Structure
from pymatgen.transformations.advanced_transformations import MonteCarloRattleTransformation
from pymatgen.analysis.ewald import EwaldSummation

#Function that removes Lithium atoms and the perturbs the Lithium removed using a MonteCarlo Transformation
#Returns a perturbed atom removed structure
def remove_li(LYC,b,RAmount):
    c = copy.deepcopy(LYC) #create copy of Full POSCAR to remove atoms from (prevents fixed changes to imported full POSCAR)
    keep = random.sample(b,RAmount) #Selects randomly which atoms to remove from a list for a fixed amount
    
    #removes the random selected atoms from the copied full POSCAR
    for i in keep:
        c.remove(i)
    
    #Performs the MonteCarlo perturbation using a minium distance (half of Li-Cl distance)
    #Aswell as a standard deviation to randomize siad minimun distance 
    trans = MonteCarloRattleTransformation(rattle_std = 0.1 , min_distance = 1.3)
    c = trans.apply_transformation(c)     
    return c

def MonteGen(FStart,FEnd,RAmount,Ts):
    #Print Direc and time
    print(FEnd)
    t = time.localtime()
    current_time = time.strftime("%H:%M:%S", t)
    print(current_time)
    
    #import POSCAR with sites full
    LYC = Structure.from_file(FStart)
    
    #Add oxidation statesto the structure required for ESum Calcualtion
    for i in LYC:
        LYC.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})

    #Create a site list containing just the lithium sites from full POSCAR
    b = []
    Li =  LYC[0].species
    for i in LYC:
        if i.species == Li:
            b.append(i) 

    #Make starting variables and lists for the generated structures
    z=0  #counter 
    Elow = 0 #Starting Elow
    Ewald_low = [0]*Ts #List which will contain Lowest Esum
    Strucs = [LYC]*Ts #Staring struc list
    
    #Make the perturbed Strucs and determine its Ewald Energy for x amount of times
    for j in range(10000):
        z = z + 1
        if (z%1000) == 0: #print progress per 1000 strucs and time
            print(z)
            print(FEnd)
            t = time.localtime()
            current_time = time.strftime("%H:%M:%S", t)
            print(current_time)
        #Create the MonteCarlo Structure
        LYCAdd = remove_li(LYC,b,RAmount)
        #Determine Ewald Energy and compares it to highest currently in the Ewald Energy List
        #If a lower Energy is found, Struc and E is added to the list
        ESum = EwaldSummation(LYCAdd) 
        if ESum.total_energy < Elow: 
            replace_low = Ewald_low.index(Elow)
            Ewald_low[replace_low] = ESum.total_energy
            Strucs[replace_low] = LYCAdd
            Elow = max(Ewald_low)
        
    #Sort Ewald from Lowest to highest, also creating an index list to sort Strucs in same order
    sort_vec = numpy.argsort(Ewald_low)
    Ewald_low.sort()
    print(Ewald_low)  
    
    #Sort Strucs in the same order as the lowest Ewald List (Low to high)
    Final_Strucs = [LYC]*Ts
    for i in range(Ts):
        a = sort_vec[i]
        Final_Strucs[i] = Strucs[a]
        
    #Generates POSCAR files from Struc list and writes it to file
    for i in range(Ts):
        j = i + 1
        f = open(FEnd+str(j), "w")
        f.write(Final_Strucs[i].to(fmt="POSCAR"))

#FileDirectory containing the POSCAR with full sites
POSCAR = r"D:\MEP\Computational\Python - generate structures from full poscar\PnmaPOSCAR\POSCAR"

#Calls Generation Function specifying: Full Start POSCAR, FinalDirec, #atomsToRemove,#POSCARS2Generate.
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li4YCl6Monte\POSCAR",0,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li3.5YCl6Monte\POSCAR",4,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li3YCl6Monte\POSCAR",8,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li2.5YCl6Monte\POSCAR",12,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li2YCl6Monte\POSCAR",16,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li1.5YCl6Monte\POSCAR",20,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li1YCl6Monte\POSCAR",24,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li0.5YCl6Monte\POSCAR",28,5)
MonteGen(POSCAR,r"D:\MEP\Computational\VASP\Relaxations\ElectrochemicalWindow\PnmaMonte\Li0YCl6Monte\POSCAR",32,5)
