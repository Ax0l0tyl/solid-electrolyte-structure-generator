import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

#importing transformations and Ewald energy calculator required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.ewald import EwaldSummation

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 

#This transformation automatically decorates a structure with oxidation states using a bond valence approach.
LYC.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})

#This computes the ewald sum Energy of a structure
ESum = EwaldSummation(LYC)
print(ESum.total_energy)



