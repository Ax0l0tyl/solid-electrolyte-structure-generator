import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

from pymatgen.vis.structure_chemview import quick_view

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 

quick_view(LYC)

