#Script to create Highest total Li distance structures by either adding or removing Lithium

from create_poscar_Total_Li import *

nr_tries =10 #How many times to try, depends on how much time it takes/time you have
nr_structures_high = 5 #The nr. of structures with lowest energy/ highest distance to write out
nr_structures_low = 1 #The nr. of structures with Smallest distance to write out
nr_structures_mid = 1 #The nr. of structures with Middle distance to write out

atoms_1 = 3 #Atoms to add or remove
element_1 = "Li" #Element to add or remove
AR = True #True to add atoms, False to remove

poscar_file = 'P-3m1'
path = "Test1"

Make_Poscar_Files(path, nr_tries, nr_structures_high, nr_structures_low, nr_structures_mid, AR, atoms_1, element_1, poscar_file)
