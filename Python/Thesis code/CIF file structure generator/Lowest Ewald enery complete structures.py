#Generates POSCAR structures with the lowest Ewald energies from a CIF file using the OrderDisorderedStructureTransformation(algo=2) function

import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.transformations.standard_transformations import OrderDisorderedStructureTransformation

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYC = LYC * (2,1,1)
#This transformation automatically decorates a structure with oxidation states using a bond valence approach.
trans = AutoOxiStateDecorationTransformation()
LYC_oxi = trans.apply_transformation(LYC)

#Order a disordered structure. The disordered structure must be oxidation state decorated for ewald sum to be computed. 
#No attempt is made to perform symmetry determination to reduce the number of combinations.
#Can generate a list of best structures, currently selects the best calculated
trans = OrderDisorderedStructureTransformation(algo=2)
LYC_Pred = trans.apply_transformation(LYC_oxi, return_ranked_list=10)

#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(len(LYC_Pred)):
    print(LYC_Pred[i]["energy"])
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(LYC_Pred[i]["structure"].to(fmt="POSCAR"))


