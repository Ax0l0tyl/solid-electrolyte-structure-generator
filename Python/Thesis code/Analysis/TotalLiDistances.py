#Analyses the total Li-Li distance of multiple structures from a file directory

def frac2car(lattice, frac):
    #Converts fractional coordinates to cartesian coordinates
    import numpy    
    cart = numpy.dot(frac,lattice)
    
    return cart


def calc_dist(lattice, frac1, frac2):
# Calculate distance (in Angstrom) between two sets of fractional coordinates 
# including PBC!
    import numpy
    cart1 = frac2car(lattice, frac1)
    cart2 = frac2car(lattice, frac2)

    temp = [None]*3
    dist = 99999.999
    for i in [-1,0,1]: #PBC in a-direction
        for j in [-1,0,1]: #PBC in b
            for k in [-1,0,1]: #PBC in c
                temp = cart2 + numpy.dot([i, j, k],lattice)  
                dist_temp = numpy.sqrt((cart1[0]-temp[0])**2 +(cart1[1]-temp[1])**2 +(cart1[2]-temp[2])**2)
                if dist_temp < dist:
                    dist = dist_temp

    return dist

def calc_total_dist(frac, lattice):
    #Calculates the electrostatic energy using the shifted potential method
    frac1 = [None]*3
    frac2 = [None]*3

    dist_tot = 0.0
    for i in range(len(frac)): #Change to loop only over Li-atoms
        frac1 = frac[i]
        for j in range(i,len(frac)): #start at i to avoid double counting
            frac2 = frac[j]
            dist_tot += calc_dist(lattice, frac1, frac2)            
            
    return dist_tot


import os
from pymatgen import Structure
import easygui 

#import Li3YCl6 from cif fileimport easygui
Num = 5
DirecListLi = []
Direc = easygui.diropenbox()
DirecList = os.listdir(Direc)

#Create a directory list of folders containing Li
for i in DirecList:
    if "Li" in i:
        DirecListLi.append(i)

#For each directory, find the total Li-Li distance per structure and print this value
for x in DirecListLi:
    print(x)
    bad = False
    for i in range(Num):
        j = i + 1
        Struc = str(Direc + "\\" + x + "\\" + str(j) + "\POSCAR")
        try:
            #import structure from POSCAR
            LYC = Structure.from_file(Struc) # place directory if no easygui 
            LiFracs = []
            #Find and make a list of Litium coordinates
            for k in LYC:
                if "Li" in k.species:
                    LiFracs.append(k.frac_coords)
            #Find lattice values
            a =LYC.lattice
            #Determine the total Li-LI distance and print value
            print(calc_total_dist(LiFracs, a.matrix))
         #Check for errors in case file is missing or no distance value is available
        except FileNotFoundError: 
            print('Failed:')
            print(x)
            bad = True
            break
        except ValueError: 
            print('EmptyPOSCAR:')
            print(x,j)
            continue
        if bad:
            continue
    



