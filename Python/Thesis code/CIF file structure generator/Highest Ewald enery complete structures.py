#Generates POSCAR structures with the highest Ewald energies from a CIF file using the OrderDisorderedStructureTransformation(algo=2) function

import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from pymatgen.analysis.structure_matcher import StructureMatcher

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.transformations.standard_transformations import OrderDisorderedStructureTransformation

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # place directory if no easygui 
LYC = LYC * (2,1,1)

#This transformation automatically decorates a structure with oxidation states using a bond valence approach.
trans = AutoOxiStateDecorationTransformation()
LYC_oxi = trans.apply_transformation(LYC)

#Order a disordered structure. The disordered structure must be oxidation state decorated for ewald sum to be computed. 
#No attempt is made to perform symmetry determination to reduce the number of combinations.
#Can generate a list of best structures, currently selects the best calculated
trans = OrderDisorderedStructureTransformation(algo=2)
LYC_Pred = trans.apply_transformation(LYC_oxi, return_ranked_list=10000)

LYCEND = []
for i in range(len(LYC_Pred)-40,len(LYC_Pred)):
    LYCEND.append(LYC_Pred[i])

#Find Symmetrical structure duplicates
N=[]
for i in range(len(LYCEND)):
     for j in range(i+1,len(LYCEND)):
        if  StructureMatcher().fit(LYCEND[i]["structure"],LYCEND[j]["structure"]):
            print(i+1,"equals",j+1) 
            N.append(j)

#Remove duplicates from Structure List
N=list(dict.fromkeys(N))
for index in sorted(N, reverse=True):
    del LYCEND[index]

#generates POSCAR file and writes to it
j=0
fname = easygui.filesavebox(default="POSCAR")
for i in range(len(LYCEND)-5,len(LYCEND)):
    print(LYCEND[i]["energy"])
    j = j + 1
    f = open(fname+str(j), "w")
    f.write(LYCEND[i]["structure"].to(fmt="POSCAR"))

