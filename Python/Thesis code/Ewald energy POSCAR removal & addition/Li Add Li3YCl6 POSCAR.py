#Pymatgen/VASP Lithium Addition POSCARs with Li3YCl6 as starting point

import numpy
import pymatgen
from pymatgen import Structure
import easygui #To open files (can also be done manually if not installed (pip install easygui))
from easygui import buttonbox

#importing transformations required
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.ewald import EwaldSummation

def add_li(LYC,b):
    #Add Lithium sites to the structure
    import random
    import copy

    #create a copy of LYC to keep LYC contact
    c = copy.deepcopy(LYC)
    #Select Lithium sites from Li List to add
    keep = random.sample(b,4)
    #Add sites to the structure
    for i in keep:
        c.insert(0,'Li',i,coords_are_cartesian=True)
        c.add_oxidation_state_by_element({"Li": 1, "Y": 3, "Cl":-1})
    return c

#import Li3YCl6 from cif fileimport easygui
LYC = Structure.from_file(easygui.fileopenbox()) # Import lowest Ewald Energy Li3YCl6 POSCAR
LYCFull = Structure.from_file(easygui.fileopenbox()) #Import Li4YCl6 POSCAR
z=0 #Counter
Li =  LYC[0].species
b = [] # Array of Lithium coordinates of the still empty sites
Ts = 5 #Total amount of structures to generate
PA= 5 #Amount to write to file

Ewald_low = [0]*Ts #Lowest Esum
Strucs = [LYC]*Ts
Final_Strucs = [LYC]*Ts

#Find the Lithium coordinates of the sites that are not yet filled by the Li3YCl6 structure
for i in range(len(LYCFull)):
    if LYCFull[i].species == Li:
        Test = True
        for j in range(len(LYC)):
            if LYC[j] == LYCFull[i]:
                Test = False
                continue
        if Test == True:  
            b.append(LYCFull[i].coords) 

#Generate new structures with sites added randomly             
for j in range(10000):
    z = z + 1
    print(z)
    #Create new strucutre using the Li3YCl6 and Li sites to be filled
    LYCAdd = add_li(LYC,b)
    #Check if structure is already in the newly generated structure list
    for k in range(Ts):
        if Strucs[k] == LYCAdd:
            break 
    #Determine Ewald energy and if this is lower than one of the energies in the list add and remove the higher energy structure        
    else:
        ESum = EwaldSummation(LYCAdd)
        for val_low in Ewald_low:
            if ESum.total_energy < val_low:
                replace_low = Ewald_low.index(max(Ewald_low))
                Ewald_low[replace_low] = ESum.total_energy
                Strucs[replace_low] = LYCAdd
                break

#Sort the structures in order of lowest to highest structures
sort_vec = numpy.argsort(Ewald_low)
Ewald_low.sort()
for i in range(Ts):
    a = sort_vec[i]
    Final_Strucs[i] = Strucs[a]
                
#generates POSCAR file and writes to it
fname = easygui.filesavebox(default="POSCAR")
for i in range(PA):
    j = i + 1
    f = open(fname+str(j), "w")
    f.write(Final_Strucs[i].to(fmt="POSCAR"))


